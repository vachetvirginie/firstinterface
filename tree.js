var Tree = /** @class */ (function () {
    function Tree(height, species, color, age) {
        this.height = height;
        this.species = species;
        this.color = color;
        this.age = age;
    }
    // render displays a tree emoji for spruce, oak, palm tree.
    Tree.prototype.render = function () {
        var emoji = this.species;
        if (this.species == "spruce") {
            emoji = "🌲";
        }
        else if (this.species == "oak") {
            emoji = "🌳";
        }
        else if (this.species == "palm") {
            emoji = "🌴";
        }
        console.log(emoji + " " + this.height + " meters " + this.color);
    };
    return Tree;
}());
var trees = [];
trees.push(new Tree(2, "spruce", "dark green", 3));
trees.push(new Tree(3, "oak", "orange", 10));
trees.push(new Tree(5, "palm", "green", 5));
for (var _i = 0, trees_1 = trees; _i < trees_1.length; _i++) {
    var t = trees_1[_i];
    t.render();
}
