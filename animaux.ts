class Animal {
    height: number;
    species: string;
    color: string;
    age: number;
    constructor(height: number, species: string, color: string, age: number) {
        this.height = height;
        this.species = species;
        this.color = color;
        this.age = age;
    }
    render() {
        let emoji = this.species;
        if (this.species == "dog") {
            emoji = "🐕"; 
        } else if (this.species == "cat") {
            emoji = "🐱"; 
        } else if (this.species == "horse") {
            emoji = "🐴"; 
        }
        console.log(emoji + " " + this.height + " meters " + this.color);
    }
}
let animaux: Animal[] = [];

animaux.push(new Animal(0.5, "dog", "blac", 3));
animaux.push(new Animal(0.3, "cat", "white", 10));
animaux.push(new Animal(1.5, "horse", "grey", 5));

for (let a of animaux) {
    a.render();
}