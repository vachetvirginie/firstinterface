var Animal = /** @class */ (function () {
    function Animal(height, species, color, age) {
        this.height = height;
        this.species = species;
        this.color = color;
        this.age = age;
    }
    Animal.prototype.render = function () {
        var emoji = this.species;
        if (this.species == "dog") {
            emoji = "🐕";
        }
        else if (this.species == "cat") {
            emoji = "🐱";
        }
        else if (this.species == "horse") {
            emoji = "🐴";
        }
        console.log(emoji + " " + this.height + " meters " + this.color);
    };
    return Animal;
}());
var animaux = [];
animaux.push(new Animal(0.5, "dog", "blac", 3));
animaux.push(new Animal(0.3, "cat", "white", 10));
animaux.push(new Animal(1.5, "horse", "grey", 5));
for (var _i = 0, animaux_1 = animaux; _i < animaux_1.length; _i++) {
    var a = animaux_1[_i];
    a.render();
}
