var ninja = /** @class */ (function () {
    function ninja() {
    }
    ninja.prototype.attack = function (f) { };
    ninja.prototype.defend = function (f) { };
    ninja.prototype.heal = function (f) { };
    ninja.prototype["throw"] = function (f) { };
    ninja.prototype.jump = function (f) { };
    return ninja;
}());
